#pragma once

#include <vector>
#include <memory>

template <typename T>
using Own = std::unique_ptr<T>;

using Scaled = float;

struct Coord
{
  Scaled x;
  Scaled y;
};

struct Dimen
{
  Scaled width;
  Scaled depth;
  Scaled height;
};

struct Node;

enum class Direction
{
  Horizontal,
  Vertical
};

struct Item
{
  virtual ~Item() = default;

  Node* label() const;
  Coord offset() const; // offset of its basepoint from its container's basepoint
  Box* container() const;
  Item* next() const;
  
  Scaled hi() const;
  Scaled lo() const;
  
  void setLabel(Node*);
  void setOffset(Coord const&);
  void setContainer(Box*);
  void setNext(Item* next);
  
  Direction principalDirection() const;
  
private:
  Node* _label;
  Coord _offset; 
  Box*  _container;
  Item* _next;
};

struct Box : public Item
{  
  virtual ~Box() = default;
};

struct Glue : public Item 
{
  virtual ~Glue() = default;
};

struct HGlue : public Glue {};
struct VGlue : public Glue {};

struct List {};

struct HList : public List {};

struct VList : public List {};

Own<Box> hbox(Own<HList>&& list);
Own<Box> hboxTo(Scaled width, Own<HList>&& list);
Own<Box> vbox(Own<VList>&& list);
Own<Box> vboxTo(Scaled size, Own<VList>&& list);

Own<HList> hlist(Own<Item>&& item);
template <typename U, typename ... T>
Own<HList> hlist(U item, T ... rest);

Own<VList> vlist(Own<Item>&& item);
template <typename U, typename ... T>
Own<VList> vlist(U item, T ... rest);

Own<VList> para(HList* list);

