#include <iostream>
#include <vector>
#include <cstdint>
#include <utility>

struct MTStyle
{
  static const MTStyle D; // display
  static const MTStyle Dc; // display cramped
  static const MTStyle T ; // text
  static const MTStyle Tc; // text cramped
  static const MTStyle S ; // script
  static const MTStyle Sc; // script cramped
  static const MTStyle SS; // scriptscript
  static const MTStyle SSc; // scriptscript cramped


  MTStyle() : MTStyle(D) {}
  MTStyle(uint8_t sty) : _sty(sty) {}
  MTStyle(MTStyle const&) = default;
  MTStyle& operator=(MTStyle const&) = default;
  
  friend
  bool operator==(MTStyle const& lhs, MTStyle const& rhs) 
  { 
    return lhs._sty == rhs._sty; 
  }
  
  bool operator<(MTStyle const& rhs) const { return _sty > rhs._sty; }

  bool isDisplayStyle() const { return *this == D || *this == Dc; }
  bool isTextStyle() const { return *this == T || *this == Tc; }
  bool isScriptStyle() const { return *this == S || *this == Sc; } 
  bool isScriptscriptStyle() const { return *this == SS || *this == SSc; }
  bool isCramped() const { return _sty % 2== 1; }
  
  MTStyle crampedStyle() const 
  {
    if (isCramped()) return *this;
    else return MTStyle(_sty + 1);
  }
  
  MTStyle supStyle() const
  {
    if (*this == D || *this== T) return S;
    if (*this == Dc || *this == Tc) return Sc;
    if (*this == S || *this == SS) return SS;
    if (*this == Sc || *this == SSc) return SSc;
    throw std::runtime_error{"should not happen"};
  }
  
  MTStyle subStyle() const
  {
    return supStyle().crampedStyle();
  }

private:
  uint8_t _sty;
};

const MTStyle MTStyle::D{0}; // display
const MTStyle MTStyle::Dc{1}; // display cramped
const MTStyle MTStyle::T {2}; // text
const MTStyle MTStyle::Tc{3}; // text cramped
const MTStyle MTStyle::S {4}; // script
const MTStyle MTStyle::Sc{5}; // script cramped
const MTStyle MTStyle::SS{6}; // scriptscript
const MTStyle MTStyle::SSc{7}; // scriptscript cramped


std::ostream& operator<<(std::ostream& os, MTStyle const& s)
{ 
  if (s == MTStyle::D) 
    os << "displaystyle"; 
  else if (s == MTStyle::Dc) 
    os << "displaystyle cramped"; 
  else if (s == MTStyle::T) 
    os << "textstyle"; 
  else if (s == MTStyle::Tc) 
    os << "textstyle cramped"; 
  else if ( s== MTStyle::S) 
    os << "scriptstyle"; 
  else if (s == MTStyle::Sc) 
    os << "scriptstyle cramped"; 
  else if ( s== MTStyle::SS) 
    os << "scriptscriptstyle";
  else if ( s== MTStyle::SSc)
    os << "scriptscriptstyle cramped"; 
  return os;
}

int main()
{
  
  using namespace std::rel_ops;
  
  MTStyle a = MTStyle::S;
  
  std::cout << a << std::endl;
  std::cout << a.crampedStyle() << std::endl;
  
  std::cout << a.supStyle() << std::endl;
  std::cout << a.subStyle() << std::endl;
  
  std::cout << (a > a.supStyle()) << std::endl;
  std::cout << (a < a.supStyle()) << std::endl;
  
}
