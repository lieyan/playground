#include <iostream>
#include <vector>
#include <fmt/format.h>
#include <fmt/ostream.h>

struct Object 
{
  virtual std::string toString() const = 0;
};

std::ostream& operator<<(std::ostream& os, Object const& o)
{
  os << o.toString();
  return os;
}

enum class LBType 
{
  Box,
  Glue,
  Penalty,
};

using Scaled  = float;

enum class GlueOrd : uint8_t
{
  normal = 0,
  fil    = 1, // first-order infinity
  fill   = 2, // second-order infinity 
  filll  = 3, // third-order infinity
};

std::ostream& operator<<(std::ostream& os, GlueOrd o)
{
  if (GlueOrd::normal == o) os << "normal";
  else if (GlueOrd::fil == o) os << "fil";
  else if (GlueOrd::fill == o) os << "fill";
  else os << "filll";
  return os;
}

using Penalty = float;
using Flag = bool;

struct GlueElasticity : Object
{
  GlueElasticity(Scaled v, GlueOrd o) : _value(v), _order(o) {}
  GlueElasticity() : GlueElasticity(0, GlueOrd::normal) {}
  Scaled value() const { return _value; }
  void value(Scaled v) { _value = v; }
  GlueOrd order() const { return _order; }
  void order(GlueOrd o) { _order = o; }
  
  std::string toString() const override
  {
    return fmt::format("new GlueElasticity({0},{1})", value(), order());
  }
private:
  Scaled _value;
  GlueOrd _order;
};

struct LBItem : Object// items for line breaking
{  
  LBItem(Scaled w) : _width(w) {}
  virtual ~LBItem() = default;
  virtual LBType lbType() const = 0;  

  Scaled width() const  { return _width; }
  void width(Scaled w) { _width = w; }

  virtual GlueElasticity stretch() const  = 0;
  virtual GlueElasticity shrink() const   = 0;
  virtual Penalty penalty() const = 0;
  virtual Flag flag() const     = 0;

private:
  Scaled _width;
};


struct LBBox : LBItem
{
  LBBox(Scaled h, Scaled d, Scaled w) : LBItem(w), _height(h), _depth(d) {}
  LBBox() : LBBox(0, 0, 0) {} 
  LBType lbType() const override { return LBType::Box; }
  GlueElasticity stretch() const override { return {}; }
  GlueElasticity shrink() const override { return {}; }
  Penalty penalty() const override { return 0; }
  Flag flag() const override { return false; }
  
  Scaled height() const { return _height; }
  void height(Scaled h) { _height = h; }
  Scaled depth() const { return _depth; }
  void depth(Scaled d) { _depth = d; }
  
  std::string toString() const override
  {
    return fmt::format("new LBBox({0},{1},{2})", height(), depth(), width());
  }
private:
  Scaled _height;
  Scaled _depth;
};


// the space between words in a line is often specified by the
// values width = 1/3 em, stretch = 1/6 em, shrink = 1/9 em
struct LBGlue : LBItem
{
  LBGlue(Scaled w, Scaled stretch, GlueOrd stretch_order, Scaled shrink, GlueOrd shrink_order)
    : LBItem(w), _stretch(stretch, stretch_order), _shrink(shrink, shrink_order) {}
  LBGlue() : LBGlue(0, 0, GlueOrd::normal, 0, GlueOrd::normal) {}
  LBType lbType() const override { return LBType::Glue; }
  
  GlueElasticity stretch() const override { return _stretch; }
  GlueElasticity shrink() const override { return _shrink; }
  void stretch(GlueElasticity e) { _stretch = e; }
  void shrink(GlueElasticity e) { _shrink = e; }
  
  Penalty penalty() const override { return 0; }
  Flag flag() const override { return false; }
  
  std::string toString() const override
  {
    return fmt::format("new LBGlue({0},{1},{2})", width(), stretch(), shrink());
  }
private:
  GlueElasticity _stretch;
  GlueElasticity _shrink;
};

struct LBPenalty : LBItem
{
  LBPenalty(Scaled w, Penalty p, Flag f) : LBItem(w), _penalty(p), _flag(f) {}
  LBPenalty() : LBPenalty(0, 0, false) {}
  
  LBType lbType() const override { return LBType::Penalty; }
  GlueElasticity stretch() const override { return {}; }
  GlueElasticity shrink() const override { return {}; }
  Penalty penalty() const override { return _penalty; }
  void penalty(Penalty p) { _penalty = p; }
  Flag flag() const override { return _flag; }
  void flag(Flag f) { _flag = f; }

  std::string toString() const override
  {
    return fmt::format("new LBPenalty({0},{1},{2})", width(), penalty(), flag());
  }

private:
  Penalty _penalty;
  Flag _flag;
};

int main()
{  
  auto p = std::make_shared<LBPenalty>(10, 1000, false);
  std::cout << *p << std::endl;
}
