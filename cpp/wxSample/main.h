//
// Created by robin on 12/30/15.
//

#ifndef PLAYGROUND_MAIN_H
#define PLAYGROUND_MAIN_H



#include <wx/wx.h>
#include <memory>
#include "Icon.h"

class MyApp : public wxApp
{
public:
  virtual bool OnInit();
};

DECLARE_APP(MyApp)

#endif //PLAYGROUND_MAIN_H
