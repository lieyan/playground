//
// Created by robin on 12/30/15.
//

#ifndef PLAYGROUND_ICON_H
#define PLAYGROUND_ICON_H

#define __ASSERT_MACROS_DEFINE_VERSIONS_WITHOUT_UNDERSCORE 0

#include <wx/wx.h>
#include <cairomm/cairomm.h>
#include <cairomm/quartz_surface.h>


class Icon : public wxFrame {
public:
  Icon(const wxString& title);

  void OnPaint(wxPaintEvent& event);

  void Render(Cairo::RefPtr<Cairo::Context> cr, int width, int height);
};


#endif //PLAYGROUND_ICON_H
