#include <iostream>
#include <memory>


#include "main.h"
#include "Icon.h"


IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{

  auto iconFrame = new Icon(wxT("Icon"));
  iconFrame->Show(true);

  return true;
}

