#include <iostream>
#include <functional>
#include <tuple>


std::tuple<int,int> floydCycleFinding(int x0, std::function<int(int)> f) {
  
  int tortoise = f(x0);
  int hare     = f(f(x0));
  while (tortoise != hare) {
    tortoise = f(tortoise);
    hare     = f(f(hare));
  }
  
  int mu = 0, hare = x0;
  while(tortoise != hare) {
    tortoise = f(tortoise);
    hare     = f(hare);
    mu ++;
  }
  
  int lambda = 1; hare = f(tortoise);
  while (tortoise != hare) {
    hare = f(hare);
    lambda ++;
  }
 
  return std::make_tuple(mu, lamdda);
}