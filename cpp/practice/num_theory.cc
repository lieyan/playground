#include <bitset>
#include <vector>
#include <cmath>
#include <tuple>

long long sieve_size;

std::bitset<10000010> bs;
std::vector<int> primes;

void sieve(long long upperbound) {
  sieve_size = upperbound + 1;
  bs.set();
  bs[0] = bs[1] = 0;
  for (long long i = 2; i <= sieve_size; i++) if (bs[i]) {
    for (long long j = i * i; j <= sieve_size; j+=i)
      bs[j] = 0;
    primes.push_back((int)i);
  }
}

// bool isPrime(long long N) {
//   if (N <= sieve_size)
//     return bs[N];
//   for (int i = 0; i < (int)primes.size(); ++i)
//     if (N % primes[i] == 0)
//       return false;
//   return true;
// }

bool isPrime(long long N) {
  if (N== 0 or N == 1)
    return false;
  if (N % 2 == 0)
    return false;
  long long sq = sqrt(N);
  for (long long i = 3; i <=sq; i+=2)
    if (N % i == 0)
      return false;
  return true;
}


int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a%b);
}

int lcm(int a, int b) {
  return a * (b / gcd(a, b));
}

std::vector<int> primeFactors(long long N) {
  std::vector<int> factors;
  long long PF_idx = 0, PF = primes[PF_idx];
  while (PF * PF <= N) {
    while (N % PF == 0) {
      N /= PF;
      factors.push_back(PF);
    }
    PF = primes[++PF_idx];
  }
  if (N != 1) factors.push_back(N);
  return factors;
}

std::tuple<int, int, int> extendedEuclid(int a, int b) {
  if (b == 0) {
    return std::make_tuple(1, 0, a);
  }
  int x, y, d;
  std::tie(x, y, d) = extendedEuclid(b, a % b);
  return std::make_tuple(y, x - (a/b)*y, d);
}

int main() {
  sieve(10000000);

  printf("%d\n", isPrime(2147483647));  
  std::vector<int> r = primeFactors(2147483647);
  for (auto x : r)
    printf("> %d\n", x);
  
  printf("%d\n", isPrime(136117223861LL));
  r = primeFactors(136117223861LL);
  for (auto x : r)
    printf("# %d\n", x);

  r = primeFactors(142391208960LL);
  for (auto x : r)
    printf("! %d\n", x);

    
  printf("%d\n", gcd(100, 142360));
  
  int x, y, d;
  std::tie(x,y,d) = extendedEuclid(100, 142360);
  printf("%d %d %d\n", x, y, d);
  
  return 0;
}



