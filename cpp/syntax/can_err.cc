#include <exception>
#include <cstddef>
#include <gsl.h>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include "can_err.h"

int main() {
  can_err(0, 
          [](int x)->bool{ return x < 0;},
          [](int x)->CanErr<int>{ printf("\nerror\n"); std::exit(-1); })
  .Do([]{
   return printf("hello");
  })
  .Do([]{
    printf(", world");
    return -2;
  })
  .Do([]{
    return printf("!\n");
  });
  
  return 0;
}