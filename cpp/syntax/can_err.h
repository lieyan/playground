#pragma once

#include <gsl.h>
#include <functional>

template <typename T>
class CanErr {
  using CheckFunc  = std::function<bool(T)>;
  using ErrHandler = std::function<CanErr<T>(T)>;

  T state;
  CheckFunc  has_error;
  ErrHandler handler;
public:  
  CanErr(T init_state, CheckFunc has_error, ErrHandler handler) 
    : state(init_state), has_error(has_error), handler(handler) {}
  
  template <typename TFunc>
  auto Do(TFunc action) {
    return Check(action());
  }
  
  auto Check(T st) {    
    state = st;
    if (has_error(state))
      return handler(state);
    else
      return *this;
  }
  
};

template <typename T, typename CheckFunc, typename ErrHandler>
CanErr<T> can_err(T init_state, CheckFunc has_error, ErrHandler handler) {
  return CanErr<T>{init_state, has_error, handler};
}

