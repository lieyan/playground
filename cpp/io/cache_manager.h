#pragma once


#include <cstddef>
#include <map>
#include "file_ref.h"
#include "blob.h"

class CacheManager {
public:
  CacheManager(size_t max_pool_size, size_t blk_size, FileRef&& file)
      : max_pool_size_(max_pool_size), blk_size_(blk_size), file_(std::move(file)) {

    for (size_t i = 0; i < max_pool_size_; ++i) {
      pool_.push_back(std::make_unique<char[]>(blk_size_));
      cache_map_.insert(std::make_pair(-1, i));
      offset_map_.insert(std::make_pair(pool_[i].get(), -1));
    }
  }

  Blob get_block(off_t blk_off) {
    Expects(blk_off % blk_size() == 0);

    auto it = cache_map_.find(blk_off);
    if (it == cache_map_.end())
      return on_cache_miss(blk_off);
    else
      return {pool_[it->second].get(), blk_size()};
  }

  void write_block(Blob blob) {
    auto it = offset_map_.find(blob.get());
    Expects(it != offset_map_.end());

    can_err(0,
            [](int state){return state == -1;},
            [](int state)->CanErr<int>{ throw std::exception{}; })
    .Do([&]{
      return lseek(file_, it->second, SEEK_SET);
    })
    .Do([&]{
      return write(file_, blob.get(), blk_size());
    });
  }

  size_t blk_size() const {
    return blk_size_;
  }
  
protected:

  Blob on_cache_miss(off_t blk_off) {
    typename std::map<off_t, size_t>::iterator victim;
    size_t blk_index;
    std::tie(victim, blk_index) = choose_victim();
    cache_map_.erase(victim, std::next(victim));
    cache_map_.insert(std::make_pair(blk_off, blk_index));

    char * buf = pool_[blk_index].get();
    can_err(0,
            [](int state){return state == -1;},
            [](int state)->CanErr<int>{ throw std::exception{}; })
    .Do([&]{
      return lseek(file_, blk_off, SEEK_SET);
    })
    .Do([&]{
      return read(file_, buf, blk_size());
    });

    offset_map_[buf] = blk_off;
    return {buf, blk_size()};
  }

  std::tuple<typename std::map<off_t, size_t>::iterator,
      size_t>
  choose_victim() {
    // TODO: implement some non-trivial strategy
    auto it = cache_map_.begin();
    return std::make_tuple(it, it->second);
  }

private:
  size_t blk_size_;
  size_t max_pool_size_;
  std::vector<std::unique_ptr<char[]>> pool_;
  std::map<off_t, size_t> cache_map_;
  std::map<char*, off_t> offset_map_;

  FileRef file_;
};


template<typename T>
T fetch(off_t offset, CacheManager& cm) {
  size_t blk_size   = cm.blk_size();
  size_t blk_num    = offset / blk_size;
  off_t  in_blk_off = offset % blk_size;
  char * buf_ptr    = cm.get_block(blk_num * blk_size).get();
  return reinterpret_cast<T>(buf_ptr + in_blk_off);
}
