#pragma once

#include <gsl.h>
#include <unistd.h>
#include <fcntl.h>
#include "can_err.h"

class FileRef {
public:
  FileRef(int fd) : fildes_(fd) {
    Expects(fildes_ != -1);
  }

  FileRef(const std::string& file_name) {
    
    can_err(0,
            [](int state){ return state == -1; },
            [&file_name](int state)->CanErr<int>{
              std::cout << "Error in opening file " << file_name << std::endl; 
              throw std::exception{}; 
            })
    .Do([this, &file_name]{
      return fildes_ = ::open(file_name.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    });
  }

  FileRef(const FileRef&)            = delete;
  FileRef& operator=(const FileRef&) = delete;
  FileRef(FileRef&& rhs) {
    close_file(fildes_);
    fildes_     = rhs.fildes_;
    rhs.fildes_ = -1;
  }
  FileRef& operator=(FileRef&& rhs) {
    close_file(fildes_);
    fildes_     = rhs.fildes_;
    rhs.fildes_ = -1;
    return *this;
  }

  ~FileRef() {
    close_file(fildes_);
  }

  operator int() const {
    return fildes_;
  }

protected:
  void close_file(int fd) {
    if (fd != -1) {
      can_err(0,
              [](int state){ return state == -1; },
              [](int state)->CanErr<int>{
                std::cout << "Error in closing file\n";
                throw std::exception{}; 
              })
      .Do([&] {
        return ::close(fd);        
      });
    }
  }
private:
  int fildes_ = -1;
};
