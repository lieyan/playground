#include <iostream>
#include <memory>
#include <cstdio>
#include <fcntl.h>
#include <unistd.h>
#include <gsl.h>
#include <map>
#include <queue>
#include <algorithm>

#include "file_ref.h"
#include "blob.h"
#include "cache_manager.h"
#include "node.h"

using namespace std;

off_t get_blk_offset(off_t offset, size_t blk_size) {
  return blk_size * (offset / blk_size);
}

off_t get_item_offset(off_t offset, size_t blk_size) {
  return offset % blk_size;
}

void write_node(CacheManager& cman, const NodeRef& node_ref, const Node & node) {
  off_t blk_offset = get_blk_offset(node_ref.offset(), cman.blk_size());
  Blob buffer = cman.get_block(blk_offset);
  memset(buffer.get(), 0, buffer.size());
  
  off_t item_offset = get_item_offset(node_ref.offset(), cman.blk_size());
  memcpy(buffer.get() + item_offset, &node, sizeof(node));
  cman.write_block(buffer);
}


int main() {

  const size_t BLK_SIZE  = 1024;
  const size_t POOL_SIZE = 10;

  CacheManager cman(POOL_SIZE, BLK_SIZE, FileRef("test.db"));

  Node node{'A', NodeRef{BLK_SIZE*1, &cman}, NodeRef{BLK_SIZE*2, &cman}};
  Node left{'B', NodeRef::null_ref(), NodeRef::null_ref()};
  Node right{'C', NodeRef::null_ref(), NodeRef::null_ref()};

  printf("write\n");

  write_node(cman, NodeRef{BLK_SIZE*0, &cman}, node);
  write_node(cman, node.left(), left);
  write_node(cman, node.right(), right);
  
  NodeRef left_ref = node.left();
  auto p = left_ref.get();
  printf("%c\n", p->data());

  return EXIT_SUCCESS;
}
