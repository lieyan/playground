TARGET=libcontest.so
OBJS=test.o  
CXX=g++
CXXFLAGS=-fPIC -m64

$(TARGET) : $(OBJS)
	$(CXX) -shared -Wl -o $(TARGET) $(OBJS)

.PHONY : clean
clean :
	rm *.o
	@echo "clean done"
