TARGET=test
OBJS=test.o trylock.o

$(TARGET) : $(OBJS)
	$(CXX) -o $(TARGET) $(OBJS)

.PHONY : clean
clean:
	rm -f $(OBJS) $(TARGET)

