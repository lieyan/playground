CXX=clang++
CXXFLAGS=
TARGET=test
OBJS=test.o

# suffix rules
%.h : %.nw
	notangle -R$@ $< > $@
%.cc : %.nw
	notangle -R$@ $< > $@

# generate target
$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS)

test.o : test.cc test.h
test.cc: test.nw
test.h : test.nw

# debug mode
.PHONY : debug
debug : CXXFLAGS+=-g -O0
debug : $(TARGET)

# weave documentation
weave : test.pdf

test.pdf: test.tex
	pdflatex test.tex
	pdflatex test.tex

test.tex: test.nw
	noweave -x test.nw > test.tex

# clean
.PHONY : clean

clean : 
	rm *.log *.aux *.o $(TARGET)

