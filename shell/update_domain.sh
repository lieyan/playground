#! /bin/sh

alias ifconfig=/sbin/ifconfig

# user name
read -p "Enter user name:" USER
# read password
read -p "Enter password:" -s PASSWD
echo

TARGETIP=`ifconfig -a | grep -w "inet" |  grep -v "127.0.0.1" | awk '{print $2;}'`
curl -su $USER:$PASSWD "http://members.3322.net/dyndns/update?system=dyndns&hostname=justcc.3322.org&myip=$TARGETIP&wildcard=OFF" > /dev/null

# print out
echo "Target IP: $TARGETIP"
echo "Update domain done"
