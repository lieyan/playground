#!/bin/sh

for i in `ls *.cue` 
do
	echo "$i.txt" > "$i.txt"
	iconv --from gbk --to utf8 $i > "$i.txt"
	mv $i "$i.old"
	mv "$i.txt" "$i"
done

